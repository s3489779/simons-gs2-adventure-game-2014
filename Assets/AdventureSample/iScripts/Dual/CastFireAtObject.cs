using UnityEngine;
using System.Collections.Generic;

public class CastFireAtObject : DualIScript
{
	public GameObject firePrefab;
	
	protected override void OnSuccess(Interactable other)
	{
		if (!other.InInventory)
		{
			GameObject fire = (GameObject)Instantiate(
				firePrefab, other.transform.position, 
				other.transform.rotation);
			
			Helpers.Game.AddObjectToScene(fire);
		}
	}
	
	protected override void OnOutOfRange(Interactable other)
	{
	}
}

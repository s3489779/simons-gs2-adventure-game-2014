using UnityEngine;

public class ExitGame : SingleIScript
{
	protected override void OnSuccess()
	{
		if (Helpers.Inventory.GetObjectCount("Egg") >= 3)
		{
			Helpers.Game.ToMainMenu();
		}
		else
		{
			Helpers.DialogueManager.SpeakNow(
				"You need 3 eggs to pass!", gameObject.renderer);
			Helpers.DialogueManager.SpeakAfter(
				"Wut...");
		}
	}
	
	protected override void OnOutOfRange()
	{
		// all other times
		if (Helpers.Game.GetFlag("failedExitDoorTwice"))
		{
			string[] lines = {
				"Ok. So I go into the closet, pick up the valve handle...",
				"Use the valve handle on the valve stem in this room...",
				"And then use the valve handle to lower the bridge. Got it!"};
			
			Helpers.DialogueManager.SpeakNow(lines);
		}
		// second time
		else if (Helpers.Game.GetFlag("failedExitDoorOnce"))
		{
			Helpers.DialogueManager.SpeakNow(
				"It seems to be connected to this valve thing...");
			
			Helpers.Game.SetFlag("failedExitDoorTwice", true);
		}
		// first time
		else
		{
			Helpers.DialogueManager.SpeakNow(
				"How can I get through that door...");
			
			Helpers.Game.SetFlag("failedExitDoorOnce", true);
		}
	}
}

using UnityEngine;
using System.Collections;

public class LookChangeDescription : BaseLook
{
	public string newDescription;
	
	protected override void OnSuccess()
	{
		base.OnSuccess();
		
		Interactable.Description = newDescription;
	}
	
	protected override void OnOutOfRange()
	{
	}
}


using UnityEngine;
using System.Collections;

public class PickUpOnceMakeUsable : PickUpOnce
{
	protected override void OnSuccess()
	{
		base.OnSuccess();
		
		SingleInteraction.VerbType verb = SingleInteraction.VerbType.Use;
		Interactable.GetInteraction(verb).AvailableInScene = true;
	}
	
	protected override void OnOutOfRange()
	{
	}
}


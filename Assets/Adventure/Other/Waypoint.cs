using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
	#if UNITY_EDITOR
	Vector3 gizmoOffsetPos = new Vector3(0, 0.5f, 0);
	
	void OnDrawGizmos()
	{
	    Gizmos.DrawIcon(transform.position + gizmoOffsetPos, "flag.png", true);
	}
	#endif
}
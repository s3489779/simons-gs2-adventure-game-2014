using UnityEngine;
using System.Collections;

[AddComponentMenu("Adventure/Player/Player")]
public class Player : MonoBehaviour
{
	#region Constants
	

	static readonly int CURSOR_PADDING = 15;
	static readonly int LINE_HEIGHT = 25;
	static readonly int AVG_CHAR_WIDTH = 9;
	
	
	#endregion
	
	
	
	#region Mode
	
	
	/// <summary>
	/// All player modes.
	/// </summary>
	enum PlayerMode
	{
		/// <summary>
		/// Player isn't doing anything interesting.
		/// </summary>
		Normal,
		/// <summary>
		/// Player is in the process of a dual interaction (e.g., combining objects).
		/// </summary>
		DualInteraction,
		/// <summary>
		/// Player is currently inside an interactable's scene menu.
		/// </summary>
		InteractableSceneMenu,
	}
	
	PlayerMode mode = PlayerMode.Normal;
	
	/// <summary>
	/// Whether player is using an interactable's scene menu.
	/// </summary>
	public bool InInteractableSceneMenu
	{ 
		get { return mode == PlayerMode.InteractableSceneMenu; }
	}
	
	/// <summary>
	/// Whether player is in the middle of a dual interaction (e.g., combining objects).
	/// </summary>
	public bool InDualInteraction
	{ 
		get { return mode == PlayerMode.DualInteraction; }
	}
	
	
	#endregion
	
	
	#region Singleton
	
	
	static Player instance;
	
	public static Player Instance
	{
		get { return instance; }
	}
	
	
	#endregion
	
	
	#region Inspector variables
	
	
	public Renderer PlayerMesh;
	
	
	#endregion
	
	
	#region Fields
	
	
	bool shouldTeleport = false;
	string teleportTargetName;

	PlayerMovement movement;
	public PlayerMovement Movement
	{
		get { return movement; }
	}
	
	/// <summary>
	/// Gets or sets the interactable currently selected by the player.
	/// </summary>
	public Interactable SelectedInteractable { get; set; }

	Inventory inventory;
	public Inventory Inventory
	{
		get { return inventory; }
	}
	
	/// <summary>
	/// Name of dual interaction verb player is currently trying to execute.
	/// </summary>	
	string DualInteractionVerb { get; set; }
	
	/// <summary>
	/// "Other" interactable in dual interaction.
	/// </summary>
	public Interactable OtherInteractable { get; set; }
	
	public InteractionScript ScriptToExecute { get; set; }
	
	
	#endregion
	
	
	#region Unity event handlers
	
	
	void Start()
	{
		// Already instantiated
		if (instance != null)
		{
			Destroy(gameObject);
			return;
		}
		
		DontDestroyOnLoad(this);
		instance = this;
		
        movement = GetComponent<PlayerMovement>();
		inventory = GetComponent<Inventory>();
	}
	
	void OnLevelWasLoaded(int level)
	{
		if (shouldTeleport)
			Teleport();
		
		mode = PlayerMode.Normal;
	}
	
	void OnGUI()
	{
		if (Event.current.type != EventType.Repaint)
			return;
		
		if (Game.Instance != null)
			GUI.skin = Game.Instance.CustomSkin;
		
		if (InDualInteraction)
			DrawDualInteractionSelectionGUI();
	}
	
	
	#endregion
	
	
	#region Teleportation
	
	
	public void TeleportOnLevelLoad(string targetName)
	{
		teleportTargetName = targetName;
		shouldTeleport = true;
	}
	
	void Teleport()
	{
		GameObject target = GameObject.Find(teleportTargetName);
		
		if (target == null)
		{
			Debug.LogWarning("Teleport failed, no target with name '" + 
				teleportTargetName + "' found in current scene.");
		}
		else
		{
			Teleport (target.transform);
		}
			
		shouldTeleport = false;
	}
	
	public void Teleport(Transform targetTransform)
	{
		transform.position = targetTransform.position;
		transform.rotation = targetTransform.rotation;
	}
	
	
	#endregion
	
	
	#region Interaction execution
	
	
	/// <summary>
	/// Executes currently set iScript and removes it from the queue.
	/// </summary>
	public void ExecuteCurrentScript()
	{
		if ((ScriptToExecute != null) && (ScriptToExecute.Interactable != null))
		{
			Interactable source = ScriptToExecute.Interactable;
			bool inRange = source.IsInInteractiveRange(transform.position);
			
			ScriptToExecute.OnInteract(inRange);
			Player.Instance.ScriptToExecute = null;
		}
		else
		{
			Debug.LogWarning("No iScript set for player to execute");
		}
	}
	
	/// <summary>
	/// Executes dual interaction, walks to object first if necessary.
	/// </summary>
	/// <remarks>Assumes "other" interactable is already set.</remarks>
	public void ExecuteDualInteraction(Interactable other)
	{
		OtherInteractable = other;
		
		// check for valid "other" object in dual interaction
		if (OtherInteractable == null)
		{
			Debug.LogWarning("No 'other' set to execute dual iScript: " + ScriptToExecute);
		}
		// check for valid source object (ie. object initiating the dual interaction)
		else if ((ScriptToExecute != null) && (ScriptToExecute.Interactable != null))
		{
			Interactable source = ScriptToExecute.Interactable;
			
			if (source == OtherInteractable)
			{				
				SwitchFromDualInteractionMode();
				OtherInteractable.SwitchToSelectedMode();
				
				Debug.Log("Cancelling dual iScript: " + ScriptToExecute);
			}
			else if (source.InInventory)
			{
				if (OtherInteractable.InInventory)
				{					
					// interact straight away
					ExecuteDualInteractionScript();
					
					inventory.TurnOffInteractableMenu();
				}
				// other in scene
				else
				{
					// walk player to 'other' and interact if possible
					Movement.MoveAndDualInteract(OtherInteractable.transform.position);
					
					SwitchToNormalMode();
					source.SwitchToNormalMode();
					inventory.TurnOffInteractableMenu();
					OtherInteractable.SwitchToSelectedMode();
					
					Debug.Log("Walking to other (" + OtherInteractable.name +
						") before running dual iScript: " + ScriptToExecute);
				}
			}
			// source in scene
			else
			{
				// walk player to 'source' and interact if possible
				Movement.MoveAndDualInteract(source.transform.position);
				
				source.SwitchFromSceneMenuMode();
				OtherInteractable.SwitchToSelectedMode();
				
				Debug.Log("Walking to source before running dual iScript: "
					+ ScriptToExecute + " with other (" + OtherInteractable.name + ")");
			}
		}
		else
		{
			Debug.LogWarning("No dual iScript set for player to execute");
		}
	}
	
	/// <summary>
	/// Executes the dual interaction script immediately.
	/// </summary>
	public void ExecuteDualInteractionScript()
	{
		// check for valid "other" object in dual interaction
		if (OtherInteractable == null)
		{
			Debug.LogWarning("No 'other' set to execute dual iScript " + ScriptToExecute);
		}
		// check for valid source object (ie. object initiating the dual interaction)
		else if ((ScriptToExecute != null) && (ScriptToExecute.Interactable != null))
		{
			Interactable source = ScriptToExecute.Interactable;
			bool inRange = false;
			
			if (source.InInventory)
			{
				if (OtherInteractable.InInventory)
				{
					inRange = true;
				}
				// other in scene
				else
				{					
					// success whether in other's interactive range
					inRange = OtherInteractable.IsInInteractiveRange(transform.position);
				}
			}
			// source in scene
			else
			{				
				// success whether in source's interactive range
				inRange = source.IsInInteractiveRange(transform.position);
			}
			
			ScriptToExecute.OnInteract(inRange);
			Player.Instance.ScriptToExecute = null;
		}
		else
		{
			Debug.LogWarning("No dual iScript set for player to execute");
		}

		OtherInteractable = null;
		SwitchFromDualInteractionMode();
	}
	
	
	#endregion
	
	
	#region Mode switching
	
	
	/// <summary>
	/// Switches player to dual interaction mode.
	/// </summary>
	public void SwitchToDualInteractionMode(DualIScript iScript, string verbName)
	{
		DualInteractionVerb = verbName;
		ScriptToExecute = iScript;
		mode = PlayerMode.DualInteraction;
		
		Debug.Log("Switched to dual interaction mode with dual iScript: " +
			iScript + " and verb (" + verbName + ")");
	}
	
	/// <summary>
	/// Switches player from dual interaction mode to normal mode.
	/// </summary>
	public void SwitchFromDualInteractionMode()
	{
		ScriptToExecute = null;
		mode = PlayerMode.Normal;
	}
	
	public void SwitchToInteractableSceneMenuMode()
	{	
		mode = PlayerMode.InteractableSceneMenu;
	}
	
	public void SwitchFromInteractableSceneMenuMode()
	{
		mode = PlayerMode.Normal;
	}
	
	public void SwitchToNormalMode()
	{
		mode = PlayerMode.Normal;
	}
	
	
	#endregion
	
	
	#region GUI helpers
	
	
	public void DrawDualInteractionSelectionGUI()
	{
		if (Event.current.type != EventType.Repaint)
			return;
		
		string label = DualInteractionVerb + ": ";
		
		if (SelectedInteractable != null)
		{
			if ((ScriptToExecute != null) && (ScriptToExecute.Interactable != null))
			{
				// if selecting source of dual interaction, click will cancel
				if (ScriptToExecute.Interactable == SelectedInteractable)
					label += "Cancel";
				else
					label += SelectedInteractable.Description;
			}
		}
		else
			label += "None";

		GUI.Box(new Rect(
			Input.mousePosition.x + CURSOR_PADDING, 
			Screen.height - Input.mousePosition.y + CURSOR_PADDING,
			(label.Length * AVG_CHAR_WIDTH),
			LINE_HEIGHT),
			label);
	}
	
	
	#endregion
}

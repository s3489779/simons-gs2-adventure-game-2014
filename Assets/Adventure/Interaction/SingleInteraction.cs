using UnityEngine;
using System;
using System.Collections;


/// <summary>
/// An interaction that can be made on a single object using a verb (e.g., using).
/// </summary>
/// <remarks>
/// Make sure every field that you want saved is marked with [SerializeField]
/// (i.e., the variable saves its value when a game is started or when opening/closing the editor)
/// </remarks>
[Serializable()]
public class SingleInteraction : Interaction
{
	#region Type
	
	
	[Flags]
	/// <summary>
	/// List of possible verbs.
	/// </summary>
	/// <remarks>
	/// Can be added to / removed at will, just make sure each has a
	/// unique power-of-two value: http://en.wikipedia.org/wiki/Bit_field)
	/// </remarks>
	public enum VerbType
	{
		Look = 1,
		Use = 2,
		PickUp = 4,
		Lay = 8,
	}
	
	[SerializeField,HideInInspector]
	VerbType verb;
	public VerbType Verb { get { return verb; } }
	public override string VerbAsString { get { return verb.ToString(); } }
	
	
	#endregion
	
	
	#region Initialisation
	
	
	public SingleInteraction(GameObject go, VerbType verb) 
		: base(go, typeof(SingleIScript), true, false)
	{		
		this.verb = verb;
	}
	
	
	#endregion
}
using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Base script for all "iScripts" that handle when player
/// interacts with an "other" interactable and object this 
/// is attached to (e.g., combining objects).
/// </summary>
public abstract class DualIScript : InteractionScript
{	
	public override void OnExecute(string verbName)
	{
		Player.Instance.SwitchToDualInteractionMode(this, verbName);
	}
	
	public override void OnInteract(bool success)
	{
		Debug.Log("Running dual iScript: " + this + " with other (" +
			Player.Instance.OtherInteractable.name + 
			") and success=" +  success);
		
		if (success)
			OnSuccess(Player.Instance.OtherInteractable);
		else
			OnOutOfRange(Player.Instance.OtherInteractable);
	}
	
	/// <summary>
	/// What to do when successfully combined with another interactable.
	/// </summary>
	protected abstract void OnSuccess(Interactable other);
	
	/// <summary>
	/// What to do when player wants to combine with another interactable but is out of range.
	/// </summary>
	protected abstract void OnOutOfRange(Interactable other);
}


using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Base class for all interactions that can be made using a "verb".
/// </summary>
/// <remarks>
/// Make sure every field that you want saved is marked with [SerializeField]
/// (i.e., the variable saves its value when a game is started or when opening/closing the editor)
/// </remarks>
[Serializable()]
public abstract class Interaction
{
	[SerializeField,HideInInspector]
	GameObject go;
	
	[SerializeField,HideInInspector]
	string iScriptConstraint;
	/// <summary>
	/// Name of class type the attached script is allowed to be.
	/// </summary>
	public string ScriptConstraint { get { return iScriptConstraint; } }
	
	public abstract string VerbAsString { get; }
	
	
	public bool AvailableInScene;
	public bool AvailableInInventory;
	

	#region iScript
	

	/// <summary>
	/// The iScript used to execute this interaction.
	/// </summary>
	[SerializeField,HideInInspector]
	InteractionScript iScript;
	public InteractionScript Script { get { return iScript; } }
	
	public string ScriptName
	{
		get
		{
			// go back to default
			if (iScript == null)
				return "None";
			else
				return iScript.GetType().ToString();
		}
		set
		{
			// didn't change
			if (value == ScriptName)
				return;
			
			RemoveScript();

			Type t = System.Type.GetType(value);
			
			if (t == null)
				return;
			
			// check for valid iScript type
			if (!t.IsSubclassOf(Type.GetType(iScriptConstraint)))
				return;
			
			InteractionScript i = AddScript(t);
			
			if (i == null)
				return;
			
			iScript = i;
		}
	}
	
	/// <summary>
	/// Add iScript of a type and return it.
	/// </summary>
	InteractionScript AddScript(Type t)
	{
		InteractionScript i = (InteractionScript)go.AddComponent(t);
		i.Interactable = (Interactable)go.GetComponent(typeof(Interactable));
		
		if (i.Interactable == null)
			return null;
		
		#if UNITY_EDITOR
		foldoutScriptInstanceParameters = true;
		foldoutScriptStaticParameters = true;
		#endif
		
		return i;
	}
	
	/// <summary>
	/// Remove / destroy script attached to this interaction.
	/// </summary>
	public void RemoveScript()
	{
		if (iScript != null)
			GameObject.DestroyImmediate(iScript);
		
		iScript = null;
	}
	
	
	#if UNITY_EDITOR
	[SerializeField,HideInInspector]
	public bool foldoutScriptStaticParameters;
	[SerializeField,HideInInspector]
	public bool foldoutScriptInstanceParameters;
	#endif
	
	
	#endregion
	
	
	#region Initialisation
	
	
	public Interaction(GameObject go, Type iScriptConstraint,
		bool availableInScene, bool availableInInventory)
	{
		this.go = go;
		this.iScriptConstraint = iScriptConstraint.ToString();
		
		this.AvailableInScene = availableInScene;
		this.AvailableInInventory = availableInInventory;
		
		iScript = null;
	}
	
	
	#endregion
}